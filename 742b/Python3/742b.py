from collections import Counter
i = input().split(' ')
numbers = input().split(' ')


def result(numbers, x):
    result = 0
    counted_numbers = Counter(map(int, numbers))
    for value in counted_numbers:
        if counted_numbers[value ^ int(x)] > 0:
            if x == '0':
                result += counted_numbers[value ^
                                          int(x)] * (counted_numbers[value] - 1)
            else:
                result += counted_numbers[value ^
                                          int(x)] * counted_numbers[value]
    return result//2


print(result(numbers, i[1]))
