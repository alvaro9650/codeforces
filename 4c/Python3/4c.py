usernames = list()
for i in range(int(input())):
    usernames.append(input())


def register(usernames):
    registered = dict()
    output = list()
    for i in usernames:
        if i in registered:
            output.append(i + str(registered[i]))
            registered[i] += 1
        else:
            output.append('OK')
            registered[i] = 1
    return output


for i in register(usernames):
    print(i)
