def dominoes(table):
    return (int(table[0]) * int(table[1])) // 2


print(dominoes(input().split(' ')))
