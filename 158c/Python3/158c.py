commands = list()
for i in range(int(input())):
    commands.append(input())


def interpreter(commands):
    output = list()
    directory = ''
    for command in commands:
        if command == 'pwd':
            output.append(directory + '/')
        else:
            subcommands = command[3:].split('/')
            i = 0
            while i < len(subcommands):
                if subcommands[i] == '..':
                    directory = directory[:directory.rfind('/')]
                elif subcommands[i] == '':
                    i += 1
                    directory = '/' + subcommands[i]
                else:
                    directory = directory + '/' + subcommands[i]
                i += 1
    return output


for i in interpreter(commands):
    print(i)
