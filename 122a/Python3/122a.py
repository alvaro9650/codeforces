luckies = [4, 7, 44, 47, 74, 77, 444, 447, 474, 477, 744, 747, 774, 777]


def isAlmostLucky(number):
    global luckies
    for lucky in luckies:
        if int(number) % lucky == 0:
            return True
    return False


print('YES' if isAlmostLucky(input()) else 'NO')
