i = input()


def sub_is_8_divisible(number):
    possible = set()
    i = 1
    while i <= len(number):
        possible.add(int(number[-i]))
        i += 1
    i = 1
    while i <= len(number):
        e = i + 1
        while e <= len(number):
            possible.add(int(number[-e] + number[-i]))
            e += 1
        i += 1
    i = 1
    while i <= len(number):
        e = i + 1
        while e <= len(number):
            o = e + 1
            while o <= len(number):
                possible.add(int(number[-o] + number[-e]+number[-i]))
                o += 1
            e += 1
        i += 1
    for i in possible:
        if int(i) % 8 == 0:
            return ['YES', i]
    return ['NO']


for i in sub_is_8_divisible(i):
    print(i)
