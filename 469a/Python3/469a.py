def possible(levels, x, y):
    return len(set(x[1:] + y[1:])) == int(levels)


print('I become the guy.' if possible(input(), input().split(
    ' '), input().split(' ')) else 'Oh, my keyboard!')
