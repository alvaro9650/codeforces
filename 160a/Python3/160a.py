def minumumToHaveMore(coins):
    total = 0
    amount = [0] * 101
    for coin in coins:
        total += int(coin)
        amount[int(coin)] += 1
    needed = total // 2 + 1
    i = 100
    have = 0
    while i * amount[i] < needed:
        have += amount[i]
        needed -= i * amount[i]
        i -= 1
    return have + needed // i + (1 if needed % i > 0 else 0)


useless = input()
print(minumumToHaveMore(input().split()))
