useless = input()


def nonDecreasingLength(numbers):
    last = -1
    maxlength = 1
    length = 0
    for i in numbers:
        if int(i) < int(last):
            maxlength = max(int(length), int(maxlength))
            length = 0
        last = i
        length += 1
    return max(int(maxlength), int(length))


print(nonDecreasingLength(input().split(' ')))
