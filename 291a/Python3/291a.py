from collections import Counter
useless = input()


def pairs(secretaries):
    counted_secretaries = Counter(secretaries)
    del counted_secretaries['0']
    while len(counted_secretaries.most_common()) > 0 and counted_secretaries.most_common()[-1][1] == 1:
        del counted_secretaries[counted_secretaries.most_common()[-1][0]]
    if len(counted_secretaries.most_common()) == 0:
        return 0
    return -1 if counted_secretaries.most_common()[0][1] > 2 else len(counted_secretaries.most_common())


print(pairs(input().split(' ')))
