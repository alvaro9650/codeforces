import operator


def solution(number):
    solutions = 0
    for a in range(1, int(number) + 1):
        for b in range(a, int(number) + 1):
            if a + b <= operator.xor(a, b) or operator.xor(a, b) < b or operator.xor(a, b) > int(number):
                continue
            else:
                solutions += 1
    return solutions


print(solution(input()))
