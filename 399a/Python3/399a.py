def stringofnumbers(start, end):
    string = ''
    for x in range(start, end):
        string += '%i ' % x
    return string


input = list(map(int, input().split(' ')))
string = ''
if input[1]-input[2] > 1:
    string += '<< '
    startingnum = input[1]-input[2]
else:
    startingnum = 1
string += stringofnumbers(startingnum, input[1])
string += '(%i) ' % input[1]
string += stringofnumbers(input[1] + 1,
                          min([input[0], input[1] + input[2]]) + 1)
if input[1] + input[2] < input[0]:
    string += '>> '
print(string)
