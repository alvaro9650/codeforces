def oneRowOneColumn(matrix):
    i = 0
    row = -1
    rowc = 0
    column = -1
    while i < len(matrix):
        if matrix[i].count('*') > 1:
            if row == -1:
                row = i
                rowc = 2
            else:
                return ['NO']
        elif matrix[i].count('*') == 1:
            if column == -1:
                column = matrix[i].index('*')
            elif column != matrix[i].index('*'):
                if row == -1:
                    row = i
                    rowc = 1
                else:
                    if rowc == 2:
                        return ['NO']
                    elif matrix[i].index('*') == matrix[row].index('*'):
                        count = 0
                        first = 0
                        e = 0
                        while e < len(matrix):
                            if matrix[e][column] == '*':
                                count += 1
                                first = e
                            e += 1
                        if count > 1:
                            return 'NO'
                        else:
                            column = matrix[i].index('*')
                            row = first
                            rowc = 2
                    else:
                        return ['NO']
        i += 1
    row = 1 if row == -1 else row + 1
    column = 1 if column == -1 else column + 1
    return ['YES', str(row) + ' ' + str(column)]


i = input().split(' ')
matrix = list()
for e in range(int(i[0])):
    matrix.append(input())
for e in oneRowOneColumn(matrix):
    print(e)
