def isWinnerFirst(sticks):
    return min(int(sticks[0]), int(sticks[1])) % 2 == 1


print('Akshat' if isWinnerFirst(input().split(' ')) else 'Malvika')
