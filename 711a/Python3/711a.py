bus = list()
possible = False
for i in range(int(input())):
    bus.append(input())
    if bus[i].count('OO') > 0:
        possible = True
if possible:
    print('YES')
    assigned = False
    for e in bus:
        if assigned:
            print(e)
        elif e[0] == 'O' and e[1] == 'O':
            assigned = True
            print('++'+e[2:])
        elif e[3] == 'O' and e[4] == 'O':
            assigned = True
            print(e[:3]+'++')
        else:
            print(e)
else:
    print('NO')
