def winner(scoring):
    scores = dict()
    for i in scoring:
        scores[i[0]] = int(scores.get(i[0], 0)) + int(i[1])
    max_score = 0
    for k, v in scores.items():
        if v > max_score:
            max_score = v
            winners = [k]
        elif v == max_score:
            winners.append(k)
    if len(winners) > 1:
        scores = dict()
        for i in scoring:
            scores[i[0]] = int(scores.get(i[0], 0)) + int(i[1])
            if i[0] in winners and scores[i[0]] >= max_score:
                return i[0]
    else:
        return winners[0]


scoring = list()
for i in range(int(input())):
    scoring.append(input().split(' '))
print(winner(scoring))
