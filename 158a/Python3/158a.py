i = input().split(' ')
participants = input().split(' ')


def winers(participants, position):
    i = int(position) - 1
    while int(i) < len(participants):
        if int(i) < 0:
            return 0
        elif int(participants[i]) < 1:
            i -= 1
        elif int(i) < len(participants) - 1 and participants[i] == participants[i + 1]:
            i += 1
        else:
            break
    return int(i) + 1


print(winers(participants, i[1]))
