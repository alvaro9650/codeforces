def solve(friends):
    if friends[0] == '1':
        if friends[1] == '1':
            return True
        elif friends[2] == '1':
            return True
    elif friends[1] == '1' and friends[2] == '1':
        return True
    return False


solved = 0
for i in range(int(input())):
    if(solve(input().split(' '))):
        solved += 1
print(solved)
