def possible(array):
    return any(x != '0' for x in array)


def arrays(array):
    arrays = (list())
    i = 1
    while i <= len(array):
        arrays.append(i)
        while i < len(array) and array[i-1] == '0':
            i += 1
        while i < len(array) and array[i] == '0':
            i += 1
        arrays[-1] = str(arrays[-1]) + ' ' + str(i)
        i += 1
    return arrays


useless = input()
array = input().split(' ')
if possible(array):
    print('YES')
    print(len(array) - array.count('0'))
    for i in arrays(array):
        print(i)
else:
    print('NO')
