def getStairs(stairs):
    return stairs.count('1')


def getHeights(stairs):
    heights = ''
    i = 1
    while i < len(stairs):
        if stairs[i] == '1':
            heights += str(stairs[i - 1]) + ' '
        i += 1
    return heights + str(stairs[-1])


useless = input()
stairs = input().split(' ')
print(getStairs(stairs))
print(getHeights(stairs))
