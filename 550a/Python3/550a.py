i = input()


def contains(string):
    ab = False
    abpos = -3
    ba = False
    i = 0
    aba = False
    if string.count('AB') == 0 or string.count('BA') == 0 or ((string.count('ABAB') == 1 or string.count('BABA') == 1) and (string.count('A') + string.count('B') == 4)):
        return 'NO'
    if string.count('AB') > 1 and string.count('BA') > 1 or (string.count('AB') > 0 and string.count('BA') > 0 and string.count('ABA') == 0 and string.count('BAB') == 0) or (string.count('AB') > 1 and string.count('BAB') == 1) or (string.count('BA') > 1 and string.count('ABA') == 1):
        return 'YES'
    while (i < len(string) - 1 or (ab and i < len(string))) and not (ab and ba):
        if string[i] == 'A':
            if not ab:
                if i < len(string) - 2 and string[i + 1] == 'B' and string[i + 2] == 'A':
                    aba = True
                    i += 2
                else:
                    ab = string[i + 1] == 'B'
                    abpos = i if ab else -3
                    if i > 0 and not ba and not ab:
                        ba = string[i - 1] == 'B'
                        if ba:
                            i += 1
            elif i > abpos + 2:
                ba = string[i - 1] == 'B'
        i += 1

    return 'YES' if ((ab or aba) and ba) or ab and (ba or aba) else 'NO'


print(contains(i))
