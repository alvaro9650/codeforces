def getdistance(orvertice, dstvertice):
    distance = 0
    actual = orvertice
    while not actual is dstvertice:
        if actual['Parent'] == -1:
            raise ValueError('Not a descendent')
        actual = actual['Parent']
        distance += 1
    return distance


vertices = list()
input()
vertices.append({'Parent': -1, 'Value': 0})
for parent in input().split(' '):
    vertices.append({'Parent': vertices[int(parent)-1], 'Value': 0})
for i in range(0, int(input())):
    query = input().split(' ')
    if int(query[0]) == 1:
        for vertice in vertices:
            try:
                vertice['Value'] += int(query[2]) - int(query[3]) * \
                    getdistance(vertice, vertices[int(query[1]) - 1])
            except Exception as e:
                pass
    else:
        print(vertices[int(query[1])-1]['Value'] % 1000000007)
