from collections import Counter
useless = input()
groups = input()


def minimum_taxis(groups):
    counted_groups = Counter(groups)
    taxis = counted_groups['4'] + counted_groups['2'] // 2
    if counted_groups['3'] >= counted_groups['1']:
        counted_groups['3'] -= counted_groups['1']
        taxis += counted_groups['1']
        counted_groups['1'] = 0
    else:
        counted_groups['1'] -= counted_groups['3']
        taxis += counted_groups['3']
        counted_groups['3'] = 0
    if counted_groups['1'] > 2 and counted_groups['2'] % 2 > 0:
        taxis += 1
        counted_groups['1'] -= 2
        counted_groups['2'] = 0
    return taxis + counted_groups['1'] // 4 + counted_groups['3'] + (1 if counted_groups['1'] % 4 > 0 or counted_groups['2'] % 2 > 0 else 0)


print(minimum_taxis(groups))
