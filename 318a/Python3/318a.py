def getNewOrder(until, position):
    if int(until) // 2 + int(until) % 2 < int(position):
        return (int(position) - (int(until) // 2 + int(until) % 2)) * 2
    else:
        return int(position) * 2 - 1


i = input().split(' ')
print(getNewOrder(i[0], i[1]))
