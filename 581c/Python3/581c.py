def maxRating(skills, units):
    rating = 0
    skills.sort()
    possible = 0
    for i in reversed(skills):
        if not (int(units) <= 0 or 10 - int(i[0]) > int(units) or int(i[1]) == 100):
            units = int(units) - (10 - int(i[0]))
            rating += 1
            possible += 9 - i[1] // 10
        else:
            possible += 10 - i[1] // 10
        rating += i[1] // 10
    return int(rating) + min(possible, int(units) // 10)


i = input().split(' ')
skills = list()
for e in input().split(' '):
    skills.append(tuple([int(e[-1]), int(e)]))
print(maxRating(skills, i[1]))
