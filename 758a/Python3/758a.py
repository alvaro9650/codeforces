input()
wealth = list(map(int, input().split(' ')))
desired = max(wealth)
needed = 0
for i in wealth:
    needed += int(desired) - int(i)
print(needed)
