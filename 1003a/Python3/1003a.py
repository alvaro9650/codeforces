input()


def returnMinimumPockets(coins):
    amount = [0] * 101
    for coin in coins:
        amount[int(coin)] += 1
    return max(amount)


print(returnMinimumPockets(input().split(' ')))
