def isWomen(nickname):
    letters = set()
    for i in nickname:
        letters.add(i)
    return len(letters) % 2 == 0


print('CHAT WITH HER!' if isWomen(input()) else 'IGNORE HIM!')
