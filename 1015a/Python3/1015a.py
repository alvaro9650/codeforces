i = input().split(' ')


def removeSegments(available, used):
    for segment in used:
        if segment in available:
            available.remove(segment)
    return available


available = list(range(1, int(i[1]) + 1))
used = set()
for i in range(int(i[0])):
    segment = input().split(' ')
    for e in range(int(segment[0]), int(segment[1]) + 1):
        used.add(e)
result = removeSegments(available, used)
print(len(result))
for i in result:
    print(i, end=' ')
