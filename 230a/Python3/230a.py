def possible(strength, dragons):
    dragons.sort()
    actualstrength = int(strength)
    for dragon in dragons:
        if actualstrength > int(dragon[0]):
            actualstrength += int(dragon[1])
        else:
            return False
    return True


i = input().split(' ')
dragons = list()
for p in range(int(i[1])):
    dragons.append(list(map(int, input().split(' '))))
print('YES' if possible(i[0], dragons) else 'NO')
