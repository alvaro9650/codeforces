def substract(number, amount):
    if int(number[-1]) + 1 == int(amount):
        print(number[:-1])
    elif int(number[-1]) + 1 > int(amount):
        print(number[:-1] + str(int(number[-1]) - int(amount)))
    else:
        substract(number[:-1], int(amount) - 1 - int(number[-1]))


i = input().split(' ')
substract(i[0], i[1])
