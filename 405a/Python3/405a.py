useless = input()


def newGravityOrder(order):
    order.sort()
    return order


for i in newGravityOrder(list(map(int, input().split(' ')))):
    print(i, end=' ')
