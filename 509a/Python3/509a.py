rows = input()
matrix = [[1 for i in range(int(rows) - 1)] for i in range(int(rows))]
for i in range(1, int(rows)):
    for e in range(int(rows) - 1):
        matrix[i][e] = matrix[i - 1][e]+matrix[i][e - 1]
if rows == '1':
    print(1)
else:
    print(matrix[-1][-1])
