friends = input()
order = input().split(' ')
given = [1] * int(friends)
for i in range(0, int(friends)):
    given[int(order[i]) - 1] = i + 1
output = str(given[0])
for i in range(1, int(friends)):
    output += ' ' + str(given[i])
print(output)
