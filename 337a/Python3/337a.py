def mimimumDifference(puzzles, amount):
    minimum = 99999999999
    puzzles.sort()
    for i in range(0, len(puzzles) - int(amount) + 1):
        if int(puzzles[i + int(amount) - 1]) - int(puzzles[i]) < minimum:
            minimum = int(puzzles[i + int(amount) - 1]) - int(puzzles[i])
    if len(puzzles) - int(amount) == 0:
        minimum = int(puzzles[-1]) - int(puzzles[0])
    return minimum


i = input().split(' ')
print(mimimumDifference(list(map(int, input().split(' '))), i[0]))
